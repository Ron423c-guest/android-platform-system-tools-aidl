NAME = aidl-cpp
SOURCES = main_cpp.cpp
LDFLAGS += -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -L$(OUT_DIR) -laidl-common \
           -L/usr/lib/$(DEB_HOST_MULTIARCH)/android -lbase

build: $(SOURCES)
	$(CXX) $^ -o $(OUT_DIR)/$(NAME) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)